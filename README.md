# npm-collection-tools-for-testing

Modules I use for writing tests

- [jsmd](https://github.com/vesln/jsmd): jsmd ensures that you will never have outdated and non-working JavaScript code in your README files.
- [lab](https://github.com/hapijs/lab): Test utility
- [mocha](https://github.com/mochajs/mocha): simple, flexible, fun test framework
- [nixt](https://github.com/vesln/nixt): Simple and powerful testing for command-line apps
- [nock](https://github.com/pgte/nock): HTTP Server mocking for Node.js
- [sinon](https://github.com/cjohansen/Sinon.JS): JavaScript test spies, stubs and mocks.
- [supertest](https://github.com/visionmedia/supertest): Super-agent driven library for testing HTTP servers
- [tap](https://github.com/isaacs/node-tap): A Test-Anything-Protocol library
